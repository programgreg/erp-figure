@echo off
REM GREGOIRE CATTAN/IHMTEK

Setlocal EnableDelayedExpansion

REM GET GIT EXECUTABLE PATH
set gitPath=%USERPROFILE%\AppData\Local\Atlassian\SourceTree\git_local\bin\git.exe

REM BY DEFAULT WE DO NOT SELECT ALL LIBRARIES
set all=0

:displaylibraries
REM WHEN ALL LIBRARIES ARE SELECTED, THE PROGRAM IS RUN SEVERAL TIME INCREASING ID IN THE SAME TIME
if "!id!" == "5" (
    set all=0
)
if "!all!" == "1" ( 
    set /a "id=!id!+1"
) else (
    REM DISPLAY LIBRARIES
    echo # Which library do you want to manage?
    echo # 1. All
    echo # 2. UBoost
    echo # 3. DevCommands
    echo # 4. VRBoost
    echo # 5. UCoreNet
    echo # 6. Other
    echo # 7. Nothing. I want to exit.
    set /p id="# Your choice (1-5): "
)

REM GET PROJECT PATH
if "!id!" == "1" (
    set all=1
    set id=2
    echo # You have selected all libraries
    echo .
)
if "!id!" == "2" (
    set name=UBoost
    set projectPath=https://gitlab.com/ihmtek/UBoost
    echo # You have selected UBoost
    goto displayactions
)
if "!id!" == "3" (
    set name=DevCommands
    set projectPath=https://gitlab.com/ihmtek/DevCommands
    echo # You have selected DevCommands
    goto displayactions
)
if "!id!" == "4" (
    set name=VRBoost
    set projectPath=https://gitlab.com/ihmtek/VRBoost
    echo # You have selected VRBoost
    goto displayactions
)
if "!id!" == "5" (
    :setucorenet
    set name=UCoreNet
    set projectPath=https://gitlab.com/programgreg/UCoreNet
    echo # You have selected UCoreNet
    goto displayactions
)
if "!id!" == "6" (
    set /p name="# Enter the exact name of the library:"
    REM MANAGE POTENTIAL ERROR IF USER ENTER UCoreNet AS IT DOES NOT HAVE THE SAME LOCATION AS OTHER PROJECTS
    if "!name!" == "UCoreNet" (
        goto setucorenet
    )
    echo # You have selected !name!
    set projectPath=https://gitlab.com/ihmtek/%name%
    goto displayactions
)
if "!id!" == "7" (
   goto endofinstall
)

REM RETURN TO MENU IF NO VALID CHOICE
cls
echo # This is not a valid choice!
echo .
goto displaylibraries

:displayactions
REM DISPLAY ACTION ONLY IF ALL LIBRARIES ARE SELECTED AND IT IS THE FIRST TIME
REM OR IF JUST ONE LIBRARY IS SELECTED
set condition=0
if "!all!"=="0" (
    set condition=1
)
if "!id!"=="2" (
    set condition=1
)
if "!condition!" == "1" (
    REM GET ACTIONS
    echo .
    echo # What do you want to do?
    echo # 1. Add library/ies
    echo # 2. Remove library/ies
    echo # 3. Update library/ies
    set /p action="# Your choice (1-3):"
)

REM ADDING GIT SUBMODULE
if "%action%" == "1" (
    echo .
    if not exist Packages/%name% (
        cd Packages
        echo # Adding submodule...
        %gitPath% submodule add --force %projectPath%
        cd ..
    ) else (
        echo %name% already exists!
    )
    goto gitupdate
)
REM REMOVING GIT SUBMODULE
if "%action%" == "2" (
    echo .
    echo # Removing submodule...
    if exist Packages/%name% (
        cd Packages
        %gitPath% submodule deinit -f -- %name%
        rd /s /q %name%
        %gitPath% rm -f %name%
        cd ..
    ) else (
        echo %name% does not exists!
    )
    echo .
    goto displaylibraries
)
REM UPDATING GIT SUBMODULE
if "%action%" == "3" (
    :gitupdate
    echo .
    echo # Configuring automatic update...
    if exist Packages/%name% (
        cd Packages
        %gitPath% config --global submodule.recurse true
        echo # Pulling current modifications...
        cd %name%
        %gitPath% pull
        cd ../..
    ) else (
        echo %name% does not exists!
    )
    echo .
    goto displaylibraries
)

REM RETURNING TO INSTALL MENU IF NO VALID CHOICE
cls
echo # This is not a valide choice!
echo .
goto displaylibraries

:endofinstall
pause