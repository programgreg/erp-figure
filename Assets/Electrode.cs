﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Electrode
	{
		public float x, y;
		public float value;

		public string name;

		static System.Random r = new System.Random();

		private void Set(float x, float y, float value, string name)
		{
			this.x = x;
			this.y = y;
			this.value = value;
			this.name = name;
		}
		public Electrode(float x, float y, string name)
		{
			Set(x, y, r.Next(-10, 10), name);
		}

		public Electrode(float x, float y, float value, string name)
		{
			Set(x, y, value, name);
		}


	}
