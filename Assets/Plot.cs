﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.Globalization;
using UCoreNet;
using UnityEngine.UI;
using System.IO;

public class Plot : MonoBehaviour {

	

	private List<string> m_sElectrodeNames;


	private Topo[] m_aTopos = new Topo[5];
	
	private int m_fNumberOfSubject = 21;
	
	UCoreNet.Plot m_canvas;
	
    void Start()
    {
		m_canvas = new UCoreNet.Plot(GLLib.s_world, new Vector2(0.05f, 0.05f), new Vector2(0.95f, 0.95f), new Vector2 (0, -4), new Vector2(1000, 4));


		TA = Read(GetCondition1Name() + "_TA.txt", GetCondition1Name() + "_TA_std.txt", GetFreq());
		NT = Read(GetCondition1Name() + "_NT.txt", GetCondition1Name() + "_NT_std.txt", GetFreq());

		conditionlabel = GetCondition1Name();
		
		m_aTopos[0] = new Topo(TA, 100, GetFreq(), 10);
		m_aTopos[1] = new Topo(TA, 200, GetFreq(), 10);
		m_aTopos[2] = new Topo(TA, 350, GetFreq(), 10);
		m_aTopos[3] = new Topo(TA, 400, GetFreq(), 10);
		m_aTopos[4] = new Topo(TA, 500, GetFreq(), 10);

		m_sElectrodeNames = m_aTopos[0].GetElectrodeNames();

		topos_label_position[0] = new Vector2(100, -3);
		topos_label_position[1] = new Vector2(200, 3);
		topos_label_position[2] = new Vector2(350, -1);
		topos_label_position[3] = new Vector2(400, 3);
		topos_label_position[4] = new Vector2(500, -1);


    }

	void ReinitTopos()
	{
		m_aTopos[0] = new Topo(TA, m_aTopos[0]);
		m_aTopos[1] = new Topo(TA, m_aTopos[1]);
		m_aTopos[2] = new Topo(TA, m_aTopos[2]);
		m_aTopos[3] = new Topo(TA, m_aTopos[3]);
		m_aTopos[4] = new Topo(TA, m_aTopos[4]);
	}

	static void DrawYLegend(string a_sText, UCoreNet.Canvas a_canvas)
	{
		GLLib.AddText(a_sText, new Vector2(-35, -1.2f), a_canvas, false);
	}
	
	public Vector3 divide(Vector3 A, Vector3 B)
	{
		return new Vector3(A.x / B.x, A.y / B.y, 0);
	}

	public Vector3 multiply(Vector3 A, Vector3 B)
	{
		return new Vector3(A.x * B.x, A.y * B.y, 0);
	}

	public Vector3 stdplus(Vector3 vector)
	{
		vector.y += vector.z;
		vector.z = 0;
		return vector;
	}

	public Vector3 stdmoins(Vector3 vector)
	{
		vector.y -= vector.z;
		vector.z = 0;
		return vector;
	}


	float MaxValue()
	{
		float max = 0;
		for(int i = 0; i < 16; ++i)
		{
			float value = MaxValue(i);
			max = value > max ? value : max;
		}
		return max;
	}

	float MinValue()
	{
		float min = 0;
		for(int i = 0; i < 16; ++i)
		{
			float value = MinValue(i);
			min = value > min ? value : min;
		}
		return min;
	}

	float MaxValue(int electrode)
	{
		float max = 0;
		foreach(Vector3 v in TA[electrode])
		{
			float value = v.y;
			max = value > max ? value : max;
		}
		return max;
	}

	float MinValue(int electrode)
	{
		float min = 0;
		foreach(Vector3 v in TA[electrode])
		{
			float value = v.y;
			min = value < min ? value : min;
		}
		return min;
	}

	Vector2 GetElectrumCentrumInTopo(Electrode a_electrode)
	{
		return new Vector2(50 + a_electrode.x / 6 * 45, 50 + a_electrode.y / 6 * 45);
	}

	void DrawElInTopo(Electrode a_electrode, UCoreNet.Canvas a_topo, float a_fWeight )
	{
		Vector2 elCentrum = GetElectrumCentrumInTopo(a_electrode);
		Vector2 extent = new Vector2(5, 0.5f) * a_fWeight;
		if(a_electrode.value > 0)
		{
			GLLib.DrawPlus(a_topo, elCentrum, extent, Color.red);
		}
		else
		{
			GLLib.DrawRect(elCentrum - extent, elCentrum + extent, a_topo, Color.blue);
		}
	}

	void DrawTopo(int a_iTopo, UCoreNet.Canvas a_canvas, Color a_color, Topo[] a_aTopos, int electrode)
	{
		Topo a_topo = a_aTopos[a_iTopo];

		UCoreNet.Canvas cTopo = new UCoreNet.Canvas(a_canvas, new Vector2(20 * a_iTopo, 0), new Vector2(20 * (a_iTopo + 1), 100), new Vector2(0, 0), new Vector2(100, 110));

		GLLib.AddText(labels_topos[a_iTopo], new Vector2(4, 110), cTopo);

		int rayon = 40;
		int cx = 50;
		int cy = 50;
		GLLib.DrawCircle(cTopo, new Vector2(cx, cy), rayon, a_color);
		GLLib.DrawLine(new Vector3(cx - rayon * 2/3.5f, cy + rayon * 2/2.5f), new Vector3(cx, cy + 2 * rayon * 2/3), cTopo, a_color); 
		GLLib.DrawLine(new Vector3(cx + rayon * 2/3.5f, cy + rayon * 2/2.5f), new Vector3(cx, cy + 2 * rayon * 2/3), cTopo, a_color); 

		float maxValue = a_topo.MaxValue();
		float minValue = a_topo.MinValue();
		
		Electrode ele = a_topo.m_aElectrode[electrode];
		Vector2 elCentrum = GetElectrumCentrumInTopo(ele);
		GLLib.DrawCircle(cTopo, elCentrum, 5, Color.black);
		
		foreach(Electrode e in a_topo.m_aElectrode)
		{
			float weight = e.value <= 0 ? e.value / minValue : e.value / maxValue;
			DrawElInTopo(e, cTopo, weight);
		}
		
	}

	void DrawTopos(UCoreNet.Canvas a_canvas, int electrode)
	{
		UCoreNet.Canvas canvasTopos = new UCoreNet.Canvas(a_canvas, new Vector2(400, -3.6f), new Vector2(980, -1.6f), new Vector2(0, 0), new Vector2(100, 100));
		GLLib.DrawRect(canvasTopos.GetMinBounds(),canvasTopos.GetMaxBounds(), canvasTopos, Color.black);
		
		DrawTopo(0, canvasTopos, Color.black, m_aTopos, electrode);
		DrawTopo(1, canvasTopos, Color.black, m_aTopos, electrode);
		DrawTopo(2, canvasTopos, Color.black, m_aTopos, electrode);
		DrawTopo(3, canvasTopos, Color.black, m_aTopos, electrode);
		DrawTopo(4, canvasTopos, Color.black, m_aTopos, electrode);
	}


	

	Vector3[][] TA;
	Vector3[][] NT;


	string GetInputFieldText(string a_sName)
	{
		try
		{
			InputField c = GameObject.Find(a_sName).GetComponent<InputField>();
			if(c.text == "" || c.text == null)
				return c.placeholder.GetComponent<Text>().text;
			return c.text;
		}
		catch(System.Exception)
		{
			return null;
		}
	}

	string GetCondition1Name()
	{
		return GetInputFieldText("Condition1");
	}

	string GetCondition2Name()
	{
		return GetInputFieldText("Condition2");
	}

	string[] GetLabels()
	{
		return GetInputFieldText("Labels").Split("-");
	}

	int GetFreq()
	{
		return int.Parse(GetInputFieldText("Freq"));
	}

	float GetStart()
	{
		return float.Parse(GetInputFieldText("Start"), CultureInfo.InvariantCulture);
	}

	float GetStop()
	{
		return float.Parse(GetInputFieldText("Stop"), CultureInfo.InvariantCulture);
	}

	Vector3[][] Read(string a_file, string a_varianceFile, int a_freq)
	{
		string[] curveFile = System.IO.File.ReadAllLines(a_file);
		string[] varFile = System.IO.File.ReadAllLines(a_varianceFile);

		int nEle = curveFile[0].Split(',').Length;
		Vector3[][] ret = new Vector3[nEle][];
		for(int j = 0; j < nEle; ++j)
		{
			ret[j] = new Vector3[curveFile.Length];
		}
		
		for(int i = 0; i < curveFile.Length; ++i)
		{
			string[] values = curveFile[i].Split(',');
			string[] stds = varFile[i].Split(',');
			if(values.Length == 1)
				continue;

			for(int j = 0; j < nEle; ++j)
			{
				float value = float.Parse(values[j], CultureInfo.InvariantCulture);
				float variance = float.Parse(stds[j], CultureInfo.InvariantCulture) / Mathf.Sqrt(m_fNumberOfSubject);
				ret[j][i] = new Vector3((float) (i + 1) / (float) a_freq * 1000f, value, variance);
			}
			
		}
		return ret;
	}

	void DrawLegend(UCoreNet.Canvas a_canvas)
	{
		Vector2 start = new Vector2(600, 1f);
		Vector2 extent = new Vector2(100, 0.5f);
		Vector2 pas = new Vector2(0, extent.y + 0.25f);

		GLLib.FillRect(start + 2  *pas, start + 2 * pas + extent, a_canvas, Color.green);
		GLLib.DrawRect(start + 2  *pas, start + 2 * pas + extent, a_canvas, Color.black);
		GLLib.AddText("target", start + 2 * pas + new Vector2(extent.x + 25, extent.y - 0.05f), a_canvas);

		GLLib.FillRect(start + pas, start + pas + extent, a_canvas, Color.red);
		GLLib.DrawRect(start + pas, start + pas + extent, a_canvas, Color.black);
		GLLib.AddText("non-target", start + pas + new Vector2(extent.x + 25, extent.y - 0.05f), a_canvas);

		GLLib.FillRect(start, start + extent, a_canvas, new Color(0, 1, 0, 0.7f));
		GLLib.FillRect(start, start + extent, a_canvas, new Color(1, 0, 0, 0.7f));
		GLLib.DrawRect(start, start + extent, a_canvas, Color.black);
		GLLib.AddText("recouvrement de", start + new Vector2(extent.x + 25, extent.y + 0.15f), a_canvas);
		GLLib.AddText("target et non-target", start + new Vector2(extent.x + 25, extent.y - 0.25f), a_canvas);
	}

	void TimeSelection(float startTime, float endTime, UCoreNet.Canvas a_canvas)
	{
		Color color = new Color(139f/255f, 0, 139/255f, 1);

		GLLib.DrawRect(new Vector2(startTime, a_canvas.GetMinBounds().y), new Vector2(endTime, a_canvas.GetMaxBounds().y), a_canvas, color);
		
		GLLib.AddText(startTime.ToString(), new Vector2(startTime - a_canvas.GetXPercent(0.02f), a_canvas.GetYPercent(0.25f)), a_canvas, color, true, true);

		GLLib.AddText(endTime.ToString(), new Vector2(endTime - a_canvas.GetXPercent(0.02f), a_canvas.GetYPercent(0.25f)), a_canvas, color, true, true);
	}
	int electrode = 6;
    void OnPostRender()
    {
		GLLib.ClearTexts();
        
		GLLib.InitRendering();

		labels_topos = GetLabels();

		m_canvas.DrawAxis();

		GLLib.DrawLine(new Vector2(0, 0), new Vector2(1000, 0), m_canvas, Color.black);
		
		for(int i = 0; i < topos_label_position.Length; ++i)
		{
			Vector3 v = topos_label_position[i];
			GLLib.AddText(labels_topos[i], v, m_canvas);
		}
		
		GLLib.DrawCurve(m_canvas, new Color(0, 1, 0, 0.7f), TA[electrode]);
		GLLib.DrawCurve(m_canvas,  new Color(1, 0, 0, 0.7f), NT[electrode]);

		TimeSelection(GetStart(), GetStop(), m_canvas);
		
		DrawYLegend("Amplitude of " + m_sElectrodeNames[electrode], m_canvas);

		
		DrawConditionLabel(conditionlabel, m_canvas);
		DrawLegend(m_canvas);

		DrawTopos(m_canvas, electrode);

	
		GLLib.CloseRendering();

    }

	string conditionlabel = "";

	void DrawConditionLabel(string a_sCondition, UCoreNet.Canvas a_canvas)
	{
		GLLib.DrawRect(new Vector2(30, 2.5f), new Vector2(80, 3.2f), a_canvas, Color.black);
		GLLib.AddText(a_sCondition, new Vector2(40, 3), a_canvas);
	}

	int figure = 0;

	Vector3[] topos_label_position = new Vector3[5];

	string[] labels_topos;

	int m_iPicNumber = 0;

	void Update()
	{
		if(Input.GetMouseButtonDown(0) && Input.GetKey(KeyCode.LeftControl))
		{
			Vector3 pos = Input.mousePosition;
			Vector3 posInPlot = GLLib.ConvertScreenCoordinate(pos, m_canvas);	
			topos_label_position[figure] = posInPlot;
			m_aTopos[figure] = new Topo(TA, (int) (posInPlot.x), GetFreq(), 10);
			figure = (figure + 1) % 5;
		}
		if(Input.GetMouseButtonDown(1))
		{
			electrode = (electrode + 1) % 16;
		}
		if(Input.GetKeyDown(KeyCode.A))
		{
			TA = Read(GetCondition1Name() + "_TA.txt", GetCondition1Name() + "_TA_std.txt", GetFreq());
			NT = Read(GetCondition1Name() + "_NT.txt", GetCondition1Name() + "_NT_std.txt", GetFreq());
			conditionlabel = GetCondition1Name();
			ReinitTopos();
		}
		if(Input.GetKeyDown(KeyCode.B))
		{
			TA = Read(GetCondition2Name() + "_TA.txt", GetCondition2Name() + "_TA_std.txt", GetFreq());
			NT = Read(GetCondition2Name() + "_NT.txt", GetCondition2Name() + "_NT_std.txt", GetFreq());
			conditionlabel = GetCondition2Name();
			ReinitTopos();
		}
		if(Input.GetKeyDown(KeyCode.P))
		{
			ScreenCapture.CaptureScreenshot("screenshot_" + m_iPicNumber + ".png");
			++m_iPicNumber;
		}
		if(Input.GetKeyDown(KeyCode.H))
		{
			var o = GameObject.Find("Canvas").GetComponent<UnityEngine.Canvas>();
			o.enabled = !o.enabled;

			//c.enabled  = !c.enabled;
		}

	}
}
