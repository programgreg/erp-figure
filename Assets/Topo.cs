﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Globalization;

public class Topo 
{

	// public Electrode fp1 = new Electrode(-1.5f, 4);
	// public Electrode fp2;
	// public Electrode fz = new Electrode(0, 2);
	// public Electrode fc5 = new Electrode(-3, 1);
	// public Electrode fc6;
	// public Electrode t7 = new Electrode(-4, 0);
	// public Electrode t8;
	// public Electrode cz = new Electrode(0, 0);
	// public Electrode p7 = new Electrode(-3.5f, -2.5f);
	// public Electrode p8;
	// public Electrode p3 = new Electrode(-1.5f, -2);
	// public Electrode p4;
	// public Electrode pZ = new Electrode(0, -2);
	// public Electrode o1;
	// public Electrode o2;
	// public Electrode oZ = new Electrode(0, -4);

	public int timeIndex, freq, window;
	public List<Electrode> m_aElectrode = new List<Electrode>();

	public List<string> GetElectrodeNames()
	{
		List<string> names = new List<string>();
		foreach(Electrode e in m_aElectrode)
		{
			names.Add(e.name);
		}
		return names;
	}

	private Electrode GetModel(string a_string, out float a_fSignOrValue)
	{
		Electrode model;
		a_string = a_string.Trim();
//		Debug.Log("++" + a_string + "--");
		if(a_string.StartsWith("-"))
		{
			model = m_aElectrode.Find(e => e.name == a_string.Remove(0, 1));
			a_fSignOrValue = -1;
		}
		else
		{
			model = m_aElectrode.Find(e => e.name == a_string);
			a_fSignOrValue = 1;
		}
		if(model == null)
			a_fSignOrValue = float.Parse(a_string, CultureInfo.InvariantCulture);
		return model;
	}

	private Electrode CastInElectrode(string a_sElectrode)
	{
		string[] attributes = a_sElectrode.Split(',');
		float signOrValue;

		string name = attributes[0].Trim();

		Electrode model = GetModel(attributes[1], out signOrValue);
		float x = model == null ? signOrValue : model.x * signOrValue;
		
		model = GetModel(attributes[2], out signOrValue);
		float y = model == null ? signOrValue : model.y * signOrValue;

		return new Electrode(x, y, name);		
	}

	private void ReadFile(string a_sFile)
	{
		IEnumerable<string> lines = System.IO.File.ReadLines(a_sFile);
		foreach(string line in lines)
		{
			m_aElectrode.Add(CastInElectrode(line));
		}
	}

	public Topo(Vector3[][] signal, Topo source):this(signal, source.timeIndex, source.freq, source.window)
	{
			
	}

	public Topo(Vector3[][] signal, int timeIndex, int freq, int window)
	{
		this.timeIndex = timeIndex;
		this.freq = freq;
		this.window = window;

		ReadFile("eeg-scalp.txt");
		// fp2 = fp1.X_Opposite();
		// fc6 = fc5.X_Opposite();
		// t8 = t7.X_Opposite();
		// p8 = p7.X_Opposite();
		// p4 = p3.X_Opposite();
		// o1 = fp1.Y_Opposite();
		// o2 = fp2.Y_Opposite();

		 int sampleIndex = (int) ((float) freq / (float) 1000 * timeIndex);
		 int windowSample = (int) ((float) freq / (float) 1000 * window);
		// fp1.value = MeanOf(signal, 0, sampleIndex, windowSample);
		// fp2.value = MeanOf(signal, 1, sampleIndex, windowSample);
		// fc5.value = MeanOf(signal, 2, sampleIndex, windowSample);
		// fz.value = MeanOf(signal, 3, sampleIndex, windowSample);
		// fc6.value = MeanOf(signal, 4, sampleIndex, windowSample);
		// t7.value = MeanOf(signal, 5, sampleIndex, windowSample);
		// cz.value = MeanOf(signal, 6, sampleIndex, windowSample);
		// t8.value = MeanOf(signal, 7, sampleIndex, windowSample);
		// p7.value = MeanOf(signal, 8, sampleIndex, windowSample);
		// p3.value = MeanOf(signal, 9, sampleIndex, windowSample);
		// pZ.value = MeanOf(signal, 10, sampleIndex, windowSample);
		// p4.value = MeanOf(signal, 11, sampleIndex, windowSample);
		// p8.value = MeanOf(signal, 12, sampleIndex, windowSample);
		// o1.value = MeanOf(signal, 13, sampleIndex, windowSample);
		// oZ.value = MeanOf(signal, 14, sampleIndex, windowSample);
		// o2.value = MeanOf(signal, 15, sampleIndex, windowSample);
		for(int i = 0; i < m_aElectrode.Count;++i)
		{
			m_aElectrode[i].value = MeanOf(signal, i, sampleIndex, windowSample);
		}


		// m_aElectrode.Add(fp1); m_aElectrode.Add(fp2); m_aElectrode.Add(fc5);
		// m_aElectrode.Add(fz); m_aElectrode.Add(fc6); m_aElectrode.Add(t7);
		// m_aElectrode.Add(cz); m_aElectrode.Add(t8); m_aElectrode.Add(p7);
		// m_aElectrode.Add(p3); m_aElectrode.Add(pZ); m_aElectrode.Add(p4);
		// m_aElectrode.Add(p8);m_aElectrode.Add(o1);m_aElectrode.Add(oZ);
		// m_aElectrode.Add(o2);

			//Interpolates();
	}

	float MeanOf(Vector3[][] signal, int electrode, int sampleIndex, int windowSample)
	{
		if(windowSample <= 1)
		{
			return signal[electrode][sampleIndex].y;
		}
		float mean = 0;
		float num = 0;
			//Debug.Log(sampleIndex  + " " + windowSample);
		for(int i = sampleIndex - windowSample / 2; i < sampleIndex + windowSample / 2; ++i)
		{
			mean += signal[electrode][i].y;
			++num;
		}

		return (float) mean / (float) num;
	}

	public void Interpolates()
	{
		
		int len = m_aElectrode.Count;
		for(int i = 0; i < len; ++i)
		{
			for(int j = 0; j < len; ++j)
			{
				if(i == j)
					continue;
				m_aElectrode.Add(Interpolate(m_aElectrode[i], m_aElectrode[j]));
			}
		}
	}

	public Electrode Interpolate(Electrode e1, Electrode e2)
	{
		return new Electrode((e1.x + e2.x)/2f, (e1.y + e2.y)/2f, (e1.value + e2.value) / 2f, e1.name + e2.name);
	}

	public float MaxValue()
	{
		float max = 0;
		foreach(Electrode e in m_aElectrode)
		{
			max = e.value > max ? e.value : max;
		}
		return max;
	}

	public float MinValue()
	{
		float min = 0;
		foreach(Electrode e in m_aElectrode)
		{
			min = e.value < min ? e.value : min;
		}
		return min;
	}
}