ABOUT
-----

erp-figure is a standalone software for comparing target and non-target erp
between two conditions.
You must provide for each condition two files named:
X_TA.txt and X_NT.txt, where X is the name of the condition.


ACTIONS
-------

Use touch:
- A or B to switch between condition A or B.
- Use 'LeftControl + Left Click' to place a label under the mouse position.
There is five ordered labels. After a click, the next label is automatically selected.
- Use the right click to select another electrode. 
There is 16 ordered electrodes. After a click, the next electrode is selected.

InputField
---------

There is five inputfields:
- The two first at the top contains the name of the conditions.
To update the view after changing the condition name, you must click on touch A or B.
The given names must mach the names of the text files.
- The two inputfields in the middle are respectively the start and stop times of the
time selection window.
- The two last ones are the names of the labels.
It must have fives names spaced by '-'.

Figure
------

On a graph we plot the curve of the target and non-target ERP with standard error
(that is, the standard deviation devided by the number of the subject).
On a topographic scalp at time t we plot the average potential of each electrode 
10 samples around time t.
A red '+' represents a positive potential. A blue '-' represents a negative potential.
Higher is the absolute amplitude of the potential, bigger is the symbol.
The amplitude is relative to the maximum or minimum averaged potential of 
all electrodes at time t.

VERSION
-------

V 0.0
